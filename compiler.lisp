(load "compiler_call.lisp")
(load "compiler_expr.lisp")

(setf indexIf 0)
(setf indexComp 0)

(defun compile (fromFile toFile)
	(let ((file (open fromFile)) (code '()) (bytecode '()))
		(loop for expr = (read file nil) while expr do
			(setf code (append code (list expr)))
		)
		(close file)

		(setf bytecode (compile_list (append code '((HALT)))))
		;;(display_code bytecode)
		(if (not (null toFile))
			(with-open-file (str (string-concat "./" toFile)
                     :direction :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
  			(format str (write-to-string bytecode)))
		)
		"Compilation: DONE !"
	)
)

(defun compile_list (listLisp)
  (if (null listLisp)
    NIL
    (append
      (compile_expr (car listLisp))
      (compile_list (cdr listLisp)))))



