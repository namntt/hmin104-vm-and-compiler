* Launch clisp by cmnd in the project directory
* (load "load") - load compiler and vm
* 
* (compile "fromFile" "toFile") - e.g: (compile "fibonaci.lisp" "fibo") 
// "toFile" must be declaire 
// "fromFile" must be *.lisp ("factorial.lisp", "fibonaci.lisp", "compiler.lisp",...) 

* (vm_create 'vm size) - set size of the vm, must be >100001 because the code starts from 100001 (recommended (vm_create 'vir 199999), higher for compiler).
* (vm_read 'vm "fileName") - load the code into memory from the 100001 address to size e.g: (vm_read 'vir "fibo")
* (vm_run 'vm) - execute the code. e.g:  (vm_run 'vir)

