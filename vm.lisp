(load "vm_memories_and_registers.lisp")
(load "vm_functions.lisp")
(load "vm_symbols.lisp")

(defun vm_create (name size)
	(setMemorySize name size)
	
	(setf (get name 'symbolR) (make-hash-table)) ; 
	(setf (get name 'referenceNR) (make-hash-table)) ; 
	
	(setf (get name 'exitVM) 0)
	(setf (get name 'maxStack) 100000)
	(setf (get name 'BP) 10)
	(setf (get name 'SP) 10)
	(setf (get name 'FP) 10)
	(setf (get name 'PC) 100001)
	(setf (get name 'R0) 0)
	(setf (get name 'R1) 0)
	(setf (get name 'R2) 0)
	
	;flag
	(setf (get name 'DEQ) 0)
	(setf (get name 'DPP) 0)
	(setf (get name 'DPG) 0)

	"VM Creation: DONE"
)


;; Gestion Symbols resolus

(defun setSymbol (vm symb adr)
	(setf (gethash symb (get vm 'symbolR)) adr)
)
(defun getSymbol (vm symb)
	(gethash symb (get vm 'symbolR))
)
(defun isSymbolSet (vm symb)
	(if (getSymbol vm symb)
		t
		nil
	)
)


(defun setReferenceNR (vm ref adr)
	(if (isReferenceSet vm ref)
		(setf (gethash ref (get vm 'referenceNR)) (cons adr (gethash ref (get vm 'referenceNR))))
		(setf (gethash ref (get vm 'referenceNR)) (list adr))
	)
)
(defun getReferenceNR (vm ref)
	(gethash ref (get vm 'referenceNR))
)
(defun isReferenceSet (vm ref)
	(if (getReferenceNR vm ref)
		t
		nil
	)
)

(defun execInst (vm value)
	(case (car value)
		('MOVE	 (vm_MOVE vm (cadr value) (caddr value)))
		('LOAD	 (vm_LOAD vm (cadr value) (caddr value)))
		('STORE	 (vm_STORE vm (cadr value) (caddr value)))
		('INCR	 (vm_INCR vm (cadr value)))
		('DECR	 (vm_DECR vm (cadr value)))
		('ADD	 (vm_ADD vm (cadr value) (caddr value)))
		('MULT	 (vm_MULT vm (cadr value) (caddr value)))
		('SUB	 (vm_SUB vm (cadr value) (caddr value)))
		('DIV	 (vm_DIV vm (cadr value) (caddr value)))
		('PUSH	 (vm_PUSH vm (cadr value)))
		('POP	 (vm_POP vm (cadr value)))
		('CMP	 (vm_CMP vm (cadr value) (caddr value)))
		('JPG	 (vm_JPG vm (cadr value)))
		('JEQ	 (vm_JEQ vm (cadr value)))
		('JPP	 (vm_JPP vm (cadr value)))
		('JGE	 (vm_JGE vm (cadr value)))
		('JPE	 (vm_JPE vm (cadr value)))
		('JMP	 (vm_JMP vm (cadr value)))
		('JSR	 (vm_JSR vm (cadr value)))
		('RTN	 (vm_RTN vm))
		('NOP	 (vm_NOP vm))
		('HALT	 (vm_HALT vm))
		('CONS	 (vm_CONS vm (cadr value) (caddr value)))
		('CAR	 (vm_CAR vm (cadr value) ))
		('CDR	 (vm_CDR vm (cadr value) ))
		(t (error "execInst unknown instruction ~s " (car value)))
	)
)

(defun vm_run (vm)
	(loop while (= (get vm 'exitVM) 0) do
		(let* ((pc (getRegister vm 'PC)) (instr (getMemoryAt vm pc)))
			(progn
				(execInst vm instr)
				(if (= (getRegister vm 'PC) pc)
					(setRegister vm 'PC (+ pc 1))
					nil
				)
			)
		)	
	)
	(print "Result: ")
	(getRegister vm 'R0)
)

(defun vm_read (vm fileName &optional (co 100001))
	(let ((file (open fileName)))
		(if file
			(prog1 
				(vm_charger vm (read file nil) co)
				(close file)
			)
		)
	)
	"Code loaded !"
)

(defun vm_charger (vm file &optional (co 100001))
	;(print file)
	(loop while (not (null file)) do
		(let ((instr (car file)))
			;(print instr)

			(if (null instr)
				nil
				(if (eql 'LABEL (car instr))
					(vm_charger_symb vm (cadr instr) co)
					(progn
						(setMemoryAt vm co (vm_resource_symb vm instr co))
						(setf co (+ co 1))
					)
				)
			)
		)
		(setf file (cdr file))
	)
)

(defun vm_charger_symb (vm symb co)
	(if (isSymbolSet vm symb)
		(error "vm_charger_symb Symbol existed")
		(progn
			(setSymbol vm symb co)
			(vm_resoudre_refNR vm symb)
		)
	)
)

(defun vm_resource_symb (vm instr co)
	(if 
		(or 
			(eql 'JMP (car instr))
			(eql 'JSR (car instr))
			(eql 'JPG (car instr))
			(eql 'JEQ (car instr))
			(eql 'JPP (car instr))
			(eql 'JGE (car instr))
			(eql 'JPE (car instr))
		)

		(if (isLABEL (cadr instr))
			(if (isSymbolSet vm (cadadr instr))
				(cons (car instr) (list (getSymbol vm (cadadr instr)))) ; met l'adresse comme integer directement ex : JSR 100152
				(progn
					(setReferenceNR vm (cadadr instr) co)
					instr
				)
			)
			instr
		)
		instr
	)
)

(defun vm_resoudre_refNR (vm symb)
	(if (isReferenceSet vm symb)
		(map
			'list
			(lambda 
				(co)
				(setMemoryAt vm co `(,(car (getMemoryAt vm co)) ,(getSymbol vm symb)))
			)
			(getReferenceNR vm symb)
		)
	)
)
