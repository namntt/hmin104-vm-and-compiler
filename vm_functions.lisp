(defun isLIT (arg)
	(and (consp arg) (eql (car arg) 'LIT))
)
(defun vm_move (vm arg1 arg2)
	(if (isLIT arg1)
		(setRegister vm arg2 (cadr arg1))
		(setRegister vm arg2 (getRegister vm arg1))
	)
)
(defun vm_load (vm adr arg)
	(if (isLIT adr)
		(vm_move vm `(LIT ,(getMemoryAt vm adr)) arg)
		(vm_move vm `(LIT ,(getMemoryAt vm (getRegister vm adr))) arg)
	)	
)
(defun vm_store (vm arg adr)
	(if (isLIT adr)
		(setMemoryAt vm adr (getRegister vm arg))
		(setMemoryAt vm (getRegister vm adr) (getRegister vm arg))
	)
)

(defun vm_incr (vm arg)
	(setRegister vm arg (+ (getRegister vm arg) 1))
)
(defun vm_decr (vm arg)
	(setRegister vm arg (- (getRegister vm arg) 1))
)

(defun vm_add (vm arg1 arg2)
	(if (isLIT arg1)
		(setRegister vm arg2 (+ (getRegister vm arg2) (cadr arg1)))
		(setRegister vm arg2 (+ (getRegister vm arg2) (getRegister vm arg1)))
	)
)
(defun vm_sub (vm arg1 arg2)
	(if (isLIT arg1)
		(setRegister vm arg2 (- (getRegister vm arg2) (cadr arg1)))
		(setRegister vm arg2 (- (getRegister vm arg2) (getRegister vm arg1)))
	)
)
(defun vm_mult (vm arg1 arg2)
	(if (isLIT arg1)
		(setRegister vm arg2 (* (getRegister vm arg2) (cadr arg1)))
		(setRegister vm arg2 (* (getRegister vm arg2) (getRegister vm arg1)))
	)
)
(defun vm_div (vm arg1 arg2)
	(if (isLIT arg1)
		(setRegister vm arg2 (/ (getRegister vm arg2) (cadr arg1)))
		(if (= 0 (getRegister arg1))
			(error "vm_div devide by 0 is impossible")
			(setRegister vm arg2 (/ (getRegister vm arg2) (getRegister vm arg1)))
		)
	)
)

(defun vm_push (vm arg)
	;(print (getRegister vm 'SP))
	(if (> (getRegister vm 'SP) (getRegister vm 'maxStack))
		(error "vm_push stack overflow")
		(progn
			(if (isLIT arg)
				(setMemoryAt vm (getRegister vm 'SP) (cadr arg))
				(setMemoryAt vm (getRegister vm 'SP) (getRegister vm arg)))
			(setRegister vm 'SP (+ (getRegister vm 'SP) 1))
		)
	)
)
(defun vm_pop (vm arg)
	;(print (getRegister vm 'SP))
	(if (<= (getRegister vm 'SP) (getRegister vm 'BP))
		(error "vm_pop stack empty")
		(progn 
			(setRegister vm 'SP (- (getRegister vm 'SP) 1))
			(setRegister vm arg (getMemoryAt vm (getRegister vm 'SP)))
		)
	)
)

(defun vm_cmp (vm arg1 arg2)
	(if (isLIT arg1)
		(setq tmpArg1 (cadr arg1))
		(setq tmpArg1 (getRegister vm arg1))
	)
	(setq tmpArg2 (getRegister vm arg2))
	(if (equal tmpArg1 tmpArg2)
		( progn
			(setRegister vm 'DEQ 1)
			(setRegister vm 'DPG 0)
			(setRegister vm 'DPP 0)
		)
		(
			if (< tmpArg1 tmpArg2)
			( progn
				(setRegister vm 'DEQ 0)
				(setRegister vm 'DPG 0)
				(setRegister vm 'DPP 1)
			)
			( progn
				(setRegister vm 'DEQ 0)
				(setRegister vm 'DPG 1)
				(setRegister vm 'DPP 0)
			)
		)
	)
)

(defun isLABEL (arg)
	(and (consp arg) (eql (car arg) 'LABEL))
)