(defun compile_expr (expr &optional env)
  (cond 
  	((consp expr)
  		(case (car expr)
  			('if (compile_if expr env))
  			('defun (compile_defun expr env))
			('halt `((HALT)))
			('nop `((NOP)))
  			(t (compile_call expr env))))
  	
  	((constantp expr) (compile_constant expr))
  	((symbolp expr) (compile_var expr env))
  	
  	(t (error "Expression ~s cannot be evaluable" expr))))


(defun compile_constant (constant) `((MOVE (LIT ,constant) R0)))

(defun compile_if (code  &optional env)
	(setf indexIf (+ indexIf 1))
	(let ((ifNot (intern (string-concat (string "IFNOT") (write-to-string indexIf))))
		 (endIf (intern (string-concat (string "ENDIF") (write-to-string indexIf)))))
		(append 
			(compile_expr (cadr code) env)
			`((CMP (LIT 0) R0))
			`((JEQ (LABEL ,ifNot)))
			(compile_expr (caddr code) env)
			`((JMP (LABEL ,endIf)))
			`((LABEL ,ifNot))
			(compile_expr (cadddr code) env)
			`((LABEL ,endIf))
		)
	)
)

(defun compile_var (var  &optional env)
	(let ((lib (assoc var env)))
		(if lib
			(append
				`((MOVE FP R0))
				`((SUB (LIT ,(cdr lib)) R0))
				`((LOAD R0 R0))
			)
			`((MOVE (VAR ,var) RO)) ; manage let
		)
	)
)

(defun compile_defun (code &optional env) 
	(let ((pilePos 0))
		(progn
			(map
				'list
				(lambda (param)
					(progn 
						(setf pilePos (+ pilePos 1))
						(setf env (acons param pilePos env))
					)
				)
				(caddr code)
			)
			(append
				`((JMP (LABEL ,(intern (string-concat "END" (string (cadr code)))))))
				`((LABEL ,(cadr code)))

				(compile_expr (cadddr code) env)

				`((MOVE FP R1))
				`((ADD (LIT 4) R1))
				'((MOVE R1 SP))
				`((RTN))

				`((LABEL ,(intern (string-concat "END" (string (cadr code))))))
			)
		)
	)
)